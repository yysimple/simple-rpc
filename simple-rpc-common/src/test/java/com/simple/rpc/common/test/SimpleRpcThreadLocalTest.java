package com.simple.rpc.common.test;

import com.simple.rpc.common.network.SimpleRpcThreadPool;
import com.simple.rpc.common.network.SimpleThreadLocal;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 项目: simple-rpc
 *
 * 功能描述:
 *
 * @author: WuChengXing
 *
 * @create: 2023-08-19 00:04
 **/
public class SimpleRpcThreadLocalTest {
    ThreadLocal<String> threadLocal = new ThreadLocal<>();
    InheritableThreadLocal<String> inheritableThreadLocal = new InheritableThreadLocal<>();
    SimpleThreadLocal<String> simpleThreadLocal = new SimpleThreadLocal<>();

    @Test
    public void threadLocalTest() {
        threadLocal.set("实际的数据");
        System.out.println("主线程：===>" + threadLocal.get());
        new Thread(() -> System.out.println("子线程：===>" + threadLocal.get()), "Sub Thread").start();
        System.out.println("主线程结束");
    }

    @Test
    public void inheriThreadLocalTest() {
        inheritableThreadLocal.set("实际的数据");
        System.out.println("主线程：===>" + inheritableThreadLocal.get());
        new Thread(() -> System.out.println("子线程：===>" + inheritableThreadLocal.get())).start();
        System.out.println("主线程结束");
    }

    @Test
    public void simpleThreadLocalTest() {
        simpleThreadLocal.set("主线程");
        System.out.println("主线程：===>" + simpleThreadLocal.get());
        new Thread(() -> System.out.println("子线程：===>" + simpleThreadLocal.get())).start();
        System.out.println("主线程结束");
    }

    @Test
    public void simpleInheriThreadLocalPoolTest() {
        inheritableThreadLocal.set("实际的数据");
        System.out.println("主线程：===>" + inheritableThreadLocal.get());
        SimpleRpcThreadPool.INSTANCE.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("子线程：===>" + inheritableThreadLocal.get());
            }
        });
        System.out.println("主线程结束");
    }

    @Test
    public void simpleThreadLocalPoolTest() throws InterruptedException {
        List<String> testList = new ArrayList<>();
        testList.add("Thread-A");
        testList.add("Thread-B");
        simpleThreadLocal.set("主线程");
        System.out.println("主线程：===>" + simpleThreadLocal.get());
        List<String> res = new ArrayList<>();
        SimpleRpcThreadPool.INSTANCE.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("子线程：===>" + simpleThreadLocal.get());
                testList.parallelStream().forEach(s -> {
                    System.out.println(s + "==" + simpleThreadLocal.get());
                    System.out.println("inner:" + Thread.currentThread());
                    res.add(s + "==" + simpleThreadLocal.get());
                });
            }
        });
        Thread.sleep(3000);
        System.out.println(res);
        System.out.println("主线程结束");

    }
}
