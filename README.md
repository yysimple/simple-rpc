# simple-rpc

#### 介绍
简单实现一个RPC：基于netty的网络通信、自定义编解码、负载均衡、配置及载器、SPI机制、心跳检测等功能；

#### 软件架构
软件架构说明


#### 安装教程

1. install之后直接引用core就行

#### 使用说明
agent: 
> -javaagent:C:\projects-code\person\java\simple-rpc\simple-rpc-agent\target\simple-rpc-agent-1.0.0.jar={\"classPrefix\":\"com.simple.rpc\",\"containRules\":\"01\",\"plugins\":\"trace\"}
> -javaagent:/Users/chengxingwu/project/person-project/simple-rpc/simple-rpc-agent/target/simple-rpc-agent-1.0.0.jar={\"classPrefix\":\"com.simple.rpc\",\"containRules\":\"01\",\"plugins\":\"trace\"}

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 具体文档

1.  系列文章其一：https://blog.csdn.net/weixin_44704261/article/details/127247964
2.  或者公众号：【爱搞技术的吴同学】同样有详细文章

