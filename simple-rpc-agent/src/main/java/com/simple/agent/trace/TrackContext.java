package com.simple.agent.trace;

import com.simple.rpc.common.network.SimpleThreadLocal;

/**
 * 项目: class-byte-code
 * <p>
 * 功能描述:
 *
 * @author: WuChengXing
 * @create: 2022-06-03 00:47
 **/
public class TrackContext {

    private static final SimpleThreadLocal<String> trackLocal = new SimpleThreadLocal<>();

    public static void clear() {
        trackLocal.remove();
    }

    public static String getTraceId() {
        return trackLocal.get();
    }

    public static void setTraceId(String traceId) {
        trackLocal.set(traceId);
    }
}
